﻿using System.IO;
using System.Text.RegularExpressions;
using System.Data;

public sealed class CsvReader : System.IDisposable
{
    public CsvReader(string fileName)
        : this(new FileStream(fileName, FileMode.Open, FileAccess.Read))
    {
    }

    public CsvReader(Stream stream)
    {
        __reader = new StreamReader(stream);
    }

    public DataSet RowEnumerator
    {
        get
        {
            if (null == __reader)
                throw new System.ApplicationException("I can't start reading without CSV input.");

            __rowno = 0;
            string sLine;
            string sNextLine;
            DataSet ds = new DataSet();
            DataTable dt = ds.Tables.Add("TheData");

            while (null != (sLine = __reader.ReadLine()))
            {

                while (rexRunOnLine.IsMatch(sLine) && null != (sNextLine = __reader.ReadLine()))
                    sLine += "\n" + sNextLine;

                __rowno++;
                DataRow dr = dt.NewRow();
                char[] delimiterChars = { '|' };
                string[] values = sLine.Split(delimiterChars);
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = Csv.Unescape(values[i]);
                    if (__rowno == 1)
                    {
                        dt.Columns.Add(values[i].Trim());
                    }
                    else
                    {
                        if (Csv.CharNotAllowes(values[i]))
                        {
                            dr[i] = values[i].Trim();
                        }

                    }
                }
                if (__rowno != 1)
                {
                    dt.Rows.Add(dr);
                }
                //yield return values;
            }
            __reader.Close();
            return ds;
        }

    }

    public long RowIndex { get { return __rowno; } }

    public void Dispose()
    {
        if (null != __reader) __reader.Dispose();
    }

    //============================================


    private long __rowno = 0;
    private TextReader __reader;
    private static Regex rexCsvSplitter = new Regex(@"|(?=(?:[^""]*""[^""]*"")*(?![^""]*""))");
    private static Regex rexRunOnLine = new Regex(@"^[^""]*(?:""[^""]*""[^""]*)*""[^""]*$");
}

public static class Csv
{
    public static string Escape(string s)
    {
        if (s.Contains(QUOTE))
            s = s.Replace(QUOTE, ESCAPED_QUOTE);

        if (s.IndexOfAny(CHARACTERS_THAT_MUST_BE_QUOTED) > -1)
            s = QUOTE + s + QUOTE;

        return s;
    }

    public static string Unescape(string s)
    {
        if (s.StartsWith(QUOTE) && s.EndsWith(QUOTE))
        {
            s = s.Substring(1, s.Length - 2);

            if (s.Contains(ESCAPED_QUOTE))
                s = s.Replace(ESCAPED_QUOTE, QUOTE);
        }

        return s;
    }
    public static bool CharNotAllowes(string s)
    {
        if (s.IndexOfAny(CHARACTERS_THAT_NOT_ALLOWED) > -1)
        {
            return false;
        }
        else
        {
            return true;
        }
    }


    private const string QUOTE = "\"";
    private const string ESCAPED_QUOTE = "\"\"";
    private static char[] CHARACTERS_THAT_MUST_BE_QUOTED = { ',', '"', '\n' };
    private static char[] CHARACTERS_THAT_NOT_ALLOWED = { '?', '!', '^', '*', '~', 'Ñ', '½', 'Ð', '', '»', 'µ', 'º', 'Ñ', '´' };
}

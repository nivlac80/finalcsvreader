using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using System.Data.Odbc;
using System.IO;
using System.Data.OleDb;

namespace Upload_Stock_Data
{
    /// <summary>
    /// Summary description for frmMain.
    /// </summary>
    public class frmMain : System.Windows.Forms.Form
    {

        # region Declarations
        private System.Windows.Forms.GroupBox gbMain;
        private System.Windows.Forms.TextBox txtCSVFolderPath;
        private System.Windows.Forms.Button btnOpenFldrBwsr;
        private System.Windows.Forms.FolderBrowserDialog fbdCSVFolder;
        private System.Windows.Forms.TextBox txtCSVFilePath;
        private System.Windows.Forms.Button btnOpenFileDlg;
        private System.Windows.Forms.OpenFileDialog openFileDialogCSVFilePath;
        private System.Windows.Forms.Button btnImport;
        private System.Windows.Forms.DataGrid dGridCSVdata;
        string strCSVFile = "";
        private System.Windows.Forms.GroupBox gbMainUploadData;
        private System.Windows.Forms.Button btnUpload;
        System.Data.Odbc.OdbcDataAdapter obj_oledb_da;
        private bool bolColName = true;
        string strFormat = "CSVDelimited";
        private System.Windows.Forms.Label lblFolderPath;
        private System.Windows.Forms.Label lblFilePath;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        # endregion

        # region Constructor

        public frmMain()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call
            //
        }
        # endregion

        # region Destructor

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        # endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbMain = new System.Windows.Forms.GroupBox();
            this.lblFilePath = new System.Windows.Forms.Label();
            this.lblFolderPath = new System.Windows.Forms.Label();
            this.dGridCSVdata = new System.Windows.Forms.DataGrid();
            this.btnImport = new System.Windows.Forms.Button();
            this.btnOpenFileDlg = new System.Windows.Forms.Button();
            this.txtCSVFilePath = new System.Windows.Forms.TextBox();
            this.btnOpenFldrBwsr = new System.Windows.Forms.Button();
            this.txtCSVFolderPath = new System.Windows.Forms.TextBox();
            this.fbdCSVFolder = new System.Windows.Forms.FolderBrowserDialog();
            this.openFileDialogCSVFilePath = new System.Windows.Forms.OpenFileDialog();
            this.gbMainUploadData = new System.Windows.Forms.GroupBox();
            this.btnUpload = new System.Windows.Forms.Button();
            this.gbMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGridCSVdata)).BeginInit();
            this.gbMainUploadData.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbMain
            // 
            this.gbMain.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMain.Controls.Add(this.lblFilePath);
            this.gbMain.Controls.Add(this.lblFolderPath);
            this.gbMain.Controls.Add(this.dGridCSVdata);
            this.gbMain.Controls.Add(this.btnImport);
            this.gbMain.Controls.Add(this.btnOpenFileDlg);
            this.gbMain.Controls.Add(this.txtCSVFilePath);
            this.gbMain.Controls.Add(this.btnOpenFldrBwsr);
            this.gbMain.Controls.Add(this.txtCSVFolderPath);
            this.gbMain.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMain.Location = new System.Drawing.Point(16, 8);
            this.gbMain.Name = "gbMain";
            this.gbMain.Size = new System.Drawing.Size(504, 416);
            this.gbMain.TabIndex = 0;
            this.gbMain.TabStop = false;
            this.gbMain.Text = "Import CSV Data";
            // 
            // lblFilePath
            // 
            this.lblFilePath.Location = new System.Drawing.Point(32, 47);
            this.lblFilePath.Name = "lblFilePath";
            this.lblFilePath.Size = new System.Drawing.Size(72, 20);
            this.lblFilePath.TabIndex = 12;
            this.lblFilePath.Text = "File Path:";
            this.lblFilePath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lblFolderPath
            // 
            this.lblFolderPath.Location = new System.Drawing.Point(32, 23);
            this.lblFolderPath.Name = "lblFolderPath";
            this.lblFolderPath.Size = new System.Drawing.Size(72, 20);
            this.lblFolderPath.TabIndex = 11;
            this.lblFolderPath.Text = "Folder Path:";
            this.lblFolderPath.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // dGridCSVdata
            // 
            this.dGridCSVdata.AlternatingBackColor = System.Drawing.SystemColors.ControlLightLight;
            this.dGridCSVdata.CaptionForeColor = System.Drawing.Color.AliceBlue;
            this.dGridCSVdata.CaptionText = "Imported CSV Data";
            this.dGridCSVdata.DataMember = "";
            this.dGridCSVdata.ForeColor = System.Drawing.Color.YellowGreen;
            this.dGridCSVdata.HeaderBackColor = System.Drawing.Color.BlanchedAlmond;
            this.dGridCSVdata.HeaderForeColor = System.Drawing.Color.Black;
            this.dGridCSVdata.Location = new System.Drawing.Point(8, 124);
            this.dGridCSVdata.Name = "dGridCSVdata";
            this.dGridCSVdata.ParentRowsForeColor = System.Drawing.Color.Yellow;
            this.dGridCSVdata.ReadOnly = true;
            this.dGridCSVdata.SelectionForeColor = System.Drawing.SystemColors.ControlLight;
            this.dGridCSVdata.Size = new System.Drawing.Size(488, 276);
            this.dGridCSVdata.TabIndex = 5;
            // 
            // btnImport
            // 
            this.btnImport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnImport.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnImport.Location = new System.Drawing.Point(112, 92);
            this.btnImport.Name = "btnImport";
            this.btnImport.Size = new System.Drawing.Size(280, 26);
            this.btnImport.TabIndex = 4;
            this.btnImport.Text = "Import CSV Data";
            this.btnImport.Click += new System.EventHandler(this.btnImport_Click);
            // 
            // btnOpenFileDlg
            // 
            this.btnOpenFileDlg.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFileDlg.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFileDlg.Location = new System.Drawing.Point(368, 47);
            this.btnOpenFileDlg.Name = "btnOpenFileDlg";
            this.btnOpenFileDlg.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFileDlg.TabIndex = 3;
            this.btnOpenFileDlg.Click += new System.EventHandler(this.btnOpenFileDlg_Click);
            // 
            // txtCSVFilePath
            // 
            this.txtCSVFilePath.BackColor = System.Drawing.SystemColors.Info;
            this.txtCSVFilePath.Location = new System.Drawing.Point(112, 48);
            this.txtCSVFilePath.Name = "txtCSVFilePath";
            this.txtCSVFilePath.ReadOnly = true;
            this.txtCSVFilePath.Size = new System.Drawing.Size(240, 20);
            this.txtCSVFilePath.TabIndex = 2;
            this.txtCSVFilePath.Text = "D:\\Test\\Test.csv";
            // 
            // btnOpenFldrBwsr
            // 
            this.btnOpenFldrBwsr.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnOpenFldrBwsr.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnOpenFldrBwsr.Location = new System.Drawing.Point(368, 24);
            this.btnOpenFldrBwsr.Name = "btnOpenFldrBwsr";
            this.btnOpenFldrBwsr.Size = new System.Drawing.Size(24, 23);
            this.btnOpenFldrBwsr.TabIndex = 1;
            this.btnOpenFldrBwsr.Click += new System.EventHandler(this.btnOpenFldrBwsr_Click);
            // 
            // txtCSVFolderPath
            // 
            this.txtCSVFolderPath.BackColor = System.Drawing.SystemColors.Info;
            this.txtCSVFolderPath.Location = new System.Drawing.Point(112, 24);
            this.txtCSVFolderPath.Name = "txtCSVFolderPath";
            this.txtCSVFolderPath.ReadOnly = true;
            this.txtCSVFolderPath.Size = new System.Drawing.Size(240, 20);
            this.txtCSVFolderPath.TabIndex = 1;
            this.txtCSVFolderPath.Text = "D:\\Test";
            // 
            // openFileDialogCSVFilePath
            // 
            //this.openFileDialogCSVFilePath.Filter = "CSV Files (*.csv)|*.csv|DAT Files (*.dat)|*.dat";
            this.openFileDialogCSVFilePath.Title = "Select the CSV file for importing";
            this.openFileDialogCSVFilePath.FileOk += new System.ComponentModel.CancelEventHandler(this.openFileDialogCSVFilePath_FileOk);
            // 
            // gbMainUploadData
            // 
            this.gbMainUploadData.BackColor = System.Drawing.SystemColors.InactiveCaptionText;
            this.gbMainUploadData.Controls.Add(this.btnUpload);
            this.gbMainUploadData.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.gbMainUploadData.Location = new System.Drawing.Point(16, 432);
            this.gbMainUploadData.Name = "gbMainUploadData";
            this.gbMainUploadData.Size = new System.Drawing.Size(504, 56);
            this.gbMainUploadData.TabIndex = 1;
            this.gbMainUploadData.TabStop = false;
            this.gbMainUploadData.Text = "Save Data in Table";
            // 
            // btnUpload
            // 
            this.btnUpload.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUpload.Enabled = false;
            this.btnUpload.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.btnUpload.Location = new System.Drawing.Point(112, 24);
            this.btnUpload.Name = "btnUpload";
            this.btnUpload.Size = new System.Drawing.Size(280, 23);
            this.btnUpload.TabIndex = 1;
            this.btnUpload.Text = "Save";
            this.btnUpload.Click += new System.EventHandler(this.btnUpload_Click);
            // 
            // frmMain
            // 
            this.AcceptButton = this.btnImport;
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(536, 494);
            this.Controls.Add(this.gbMainUploadData);
            this.Controls.Add(this.gbMain);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "CSV Reader";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMain_Closing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.gbMain.ResumeLayout(false);
            this.gbMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dGridCSVdata)).EndInit();
            this.gbMainUploadData.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        # region Main() Method
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]

        static void Main()
        {
            Application.EnableVisualStyles();
            Application.DoEvents();
            Application.Run(new frmMain());
        }
        #endregion

        #region Form Load
        private void frmMain_Load(object sender, System.EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }

        }
        # endregion

        # region Open Folder Browser Button
        // On click of this button, the FOLDERBROWSERDIALOG opens where user can select the path of the folder 
        // containing .csv files

        private void btnOpenFldrBwsr_Click(object sender, System.EventArgs e)
        {

            try
            {
                if (fbdCSVFolder.ShowDialog() == DialogResult.OK)
                {
                    txtCSVFolderPath.Text = fbdCSVFolder.SelectedPath.Trim();


                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {

            }
        }
        # endregion

        # region Open File Dialog Button

        // On click of this button, the openfiledialog opens where user can select .csv file  		

        private void btnOpenFileDlg_Click(object sender, System.EventArgs e)
        {
            try
            {
                openFileDialogCSVFilePath.InitialDirectory = txtCSVFolderPath.Text.Trim();
                if (openFileDialogCSVFilePath.ShowDialog() == DialogResult.OK) 
                {
                    txtCSVFilePath.Text = openFileDialogCSVFilePath.FileName.Trim();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        # endregion


        # region Function For Importing Data From CSV File
        public DataSet ConnectCSV()
        {
            DataSet ds = new DataSet();

            string fileName = openFileDialogCSVFilePath.FileName;

            CsvReader reader = new CsvReader(fileName);

            ds = reader.RowEnumerator;
            dGridCSVdata.DataSource = ds;

            dGridCSVdata.DataMember = "TheData";
            return ds;
        }

        # endregion

        # region Button Import CSV Data
        private void btnImport_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (txtCSVFolderPath.Text == "")
                {
                    MessageBox.Show("The Folder Path TextBox cannot be empty.", "Warning");
                    return;
                }
                else if (txtCSVFilePath.Text == "")
                {
                    MessageBox.Show("The File Path TextBox cannot be empty.", "Warning");
                    return;
                }

                else
                {
                    //int intLengthOfFileName = txtCSVFilePath.Text.Trim().Length;
                    //int intLastIndex = txtCSVFilePath.Text.Trim().LastIndexOf("\\");
                    //strCSVFile = txtCSVFilePath.Text.Trim().Substring(intLastIndex, intLengthOfFileName - intLastIndex);
                    //strCSVFile = strCSVFile.Remove(0, 1).Trim();

                    // writeSchema();
                    ConnectCSV();
                    btnUpload.Enabled = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        # endregion

        #region Button Insert Data

        // Here we will insert the imported data in our database
        private void btnUpload_Click(object sender, System.EventArgs e)
        {
            try
            {
                // Create an SQL Connection
                //SqlConnection con1 = new SqlConnection(ReadConFile().Trim());

                OleDbConnection con1 = new OleDbConnection();
                con1.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;Data Source = C:\\Users\\KML Surani\\Documents\\ImportCSV.accdb;Persist Security Info=False;";
                OleDbCommand cmd = new OleDbCommand();

                // Create Dataset					
                DataSet da = new DataSet();

                // To actually fill the dataset, Call the function ImportCSV and assign the returned 
                // dataset to new dataset as below

                da = this.ConnectCSV();

                // Now we will collect data from data table and insert it into database one by one
                // Initially there will be no data in database so we will insert data in first two columns
                // and after that we will update data in same row for remaining columns
                // The logic is simple. 'i' represents rows while 'j' represents columns



                con1.Open();
                cmd.Connection = con1;
                cmd.CommandType = CommandType.Text;
                for (int i = 0; i <= da.Tables["TheData"].Rows.Count - 1; i++)
                {

                    cmd.CommandText = "Insert  into tblImportCSV (Name,City)  values('" + da.Tables["TheData"].Rows[i]["Name"] + "','" + da.Tables["TheData"].Rows[i]["City"] + "')";
                    // For UPDATE statement, in where clause you need some unique row 
                    //identifier. We are using �srno� in WHERE clause. 
                    //cmd1.CommandText = "Update Test set " + da.Tables["Stocks"].Columns[j].ColumnName.Trim() + " = '" + da.Tables["Stocks"].Rows[i].ItemArray.GetValue(j) + "' where srno =" + (i + 1);

                    cmd.ExecuteNonQuery();
                    //cmd1.ExecuteNonQuery();

                }

                con1.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            {
                btnUpload.Enabled = false;
            }
        }
        #endregion



        #  region Form Closing
        private void frmMain_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                Application.Exit();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            finally
            { }
        }
        # endregion

        private void openFileDialogCSVFilePath_FileOk(object sender, CancelEventArgs e)
        {

        }


    }
}
